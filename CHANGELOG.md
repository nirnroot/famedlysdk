## [0.1.5] - 26 Jun 2021
- fix: Don't run syncs while the client is being initialized

## [0.1.4] - 19 Jun 2021
- change: Replace onSyncError Stream with onSyncStatus

## [0.1.3] - 19 Jun 2021
- feat: Implement migration for hive schema versions

## [0.1.2] - 19 Jun 2021
- fix: Hive breaks if room IDs contain emojis (yes there are users with hacked synapses out there who needs this)
- feat: Also migrate inbound group sessions

## [0.1.1] - 18 Jun 2021
- refactor: Move pedantic to dev_dependencies
- chore: Update readme
- fix: Migrate missing device keys

## [0.1.0] - 17 Jun 2021

First stable version
